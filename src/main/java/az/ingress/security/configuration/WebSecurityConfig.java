package az.ingress.security.configuration;

import az.ingress.security.filter.JwtAuthFilterConfigurerAdapter;
import az.ingress.security.filter.JwtAuthRequestFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

// Bunu yazanda bizden parol isdemir
@Configuration
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtAuthFilterConfigurerAdapter authFilterConfigurerAdapter;

    private final PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
       // http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.apply(authFilterConfigurerAdapter);
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/auth/sign-in")
                .permitAll()
                .antMatchers("/hello/public")
                .permitAll()
                .antMatchers("/hello/developer")
                .hasRole("developer")
                .antMatchers("/hello/admin")
                .hasAnyRole("admin" ,"ceo")
                        .antMatchers(HttpMethod.GET, "/hello/books").hasAnyRole("developer", "admin", "ceo")
                        .antMatchers(HttpMethod.POST, "/hello/books").hasAnyRole("admin");
        super.configure(http);
    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
//        auth.inMemoryAuthentication()
//                .withUser("behruz")
//                .password(passwordEncoder.encode( "1234"))
//                .roles("admin", "ceo" )
//                .and()
//                .withUser("subhan")
//                .password(passwordEncoder.encode( "1234"))
//                .roles("developer");
//    }

}
