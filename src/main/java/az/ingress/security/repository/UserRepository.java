package az.ingress.security.repository;

import az.ingress.security.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

      User findByUsername(String username);
}
