package az.ingress.security.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/hello")
@RequiredArgsConstructor
public class HelloController {

    @GetMapping("/public")
    public String sayHelloPublic(){
        return "Hello world to Everyone";
    }

    @GetMapping("/user")
    public String sayHello(Authentication authentication){
     //  Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return "Hello , " + authentication.getName() + " !";
    }

    @PostMapping("/post")
    public String createUser(){
        return "User is created";
    }


    @GetMapping("/developer")
    public String sayHelloDev(Authentication authentication){
        List<? extends GrantedAuthority> collect = authentication.getAuthorities()
                .stream().collect(Collectors.toList());
        return "Hello developer! " + authentication.getName()+" authorities" + collect;
    }

    @GetMapping("/admin")
    public String sayHelloAdmin(Authentication authentication){
        List<? extends GrantedAuthority> collect = authentication.getAuthorities()
                .stream().collect(Collectors.toList());
        return "Hello admin! " + authentication.getName()+" authorities" + collect;
    }

    @GetMapping("/books")
    public String listBooks(Authentication authentication){
        List<? extends GrantedAuthority> collect = authentication.getAuthorities()
                .stream().collect(Collectors.toList());
        return "List of some books! " + authentication.getName()+" authorities" + collect;
    }

    @PostMapping("/books")
    public String createBook(Authentication authentication){
        List<? extends GrantedAuthority> collect = authentication.getAuthorities()
                .stream().collect(Collectors.toList());
        return "Create of some books " + authentication.getName()+" authorities" + collect;
    }
}
